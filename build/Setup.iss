#define MyAppName "Trenclo App"
#define MyAppVersion "0.0.1"
#define MyAppPublisher "Trenclo Ltd"
#define MyAppURL "https://www.trenclo.com"

[Setup]
AppId={{1DA562BE-1E03-4330-A43A-5658E8E21A1B}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={autopf}\Trenclo\App
DefaultGroupName=Trenclo\App
DisableProgramGroupPage=yes
LicenseFile=C:\Projects\Trenclo\TrencloApp\LICENSE
PrivilegesRequiredOverridesAllowed=dialog
OutputDir=C:\Projects\Trenclo\TrencloApp\build
OutputBaseFilename=TrencloAppSetup
SetupIconFile=C:\Projects\Trenclo\TrencloApp\icon.ico
Compression=lzma
SolidCompression=yes
WizardStyle=modern
VersionInfoVersion=0.0.1
VersionInfoCompany=Trenclo Ltd
VersionInfoCopyright=Trenclo Ltd
VersionInfoProductName=Trenclo App
VersionInfoProductVersion=0.0.1
MinVersion=0,6.1
AppCopyright=Trenclo Ltd
UninstallDisplayName=Trenclo App

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Files]
Source: "..\res\*"; DestDir: "{app}\res\"; Flags: ignoreversion createallsubdirs recursesubdirs
Source: "..\www\*"; DestDir: "{app}\www\"; Flags: ignoreversion createallsubdirs recursesubdirs
Source: "..\..\..\..\Users\Josh Plant\Documents\nwjs-v0.45.4-win-x64\*"; DestDir: "{app}"; Flags: ignoreversion createallsubdirs recursesubdirs
Source: "..\icon.ico"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\icon.png"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\LICENSE"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\package.json"; DestDir: "{app}"; Flags: ignoreversion

[Icons]
Name: "{group}\Uninstall Trenclo App"; Filename: "{uninstallexe}"; IconFilename: "{app}\icon.ico"; IconIndex: 0
Name: "{group}\Trenclo App"; Filename: "{app}\nw.exe"; IconFilename: "{app}\icon.ico"; IconIndex: 0

[Run]
Filename: {app}\nw.exe; Description: {cm:LaunchProgram,{cm:AppName}}; Flags: nowait postinstall skipifsilent

[CustomMessages]
AppName=Trenclo App
LaunchProgram=Launch Trenclo App