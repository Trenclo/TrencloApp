$('#data-progress-bar').show();

apiRequest('GET', '/resources/domains/'+localStorage.getItem('SelectedObjectID'), [], function(data){
    $('#data-progress-bar').hide();
    $('#dataList').text('');

    if(Object.keys(data.data).length > 0){

        $("#domain").text(data.data.domainname);
        $("#status").text(data.data.status);
        $("#created").text(data.data.timecreated);

        console.log(data.data);


    }else{
        ons.notification.alert('An error occured while fetching your domain details.').then(function(){
            $('#data-progress-bar').hide();
        });
    }

}, function(error){
    ons.notification.alert('An error occured while fetching your domain details.').then(function(){
        $('#data-progress-bar').hide();
    });

}, function(){
    ons.notification.alert('The request timed out. Please try again or check your internet connection.').then(function(){
        $('#data-progress-bar').hide();
    });
});