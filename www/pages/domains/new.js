$('#checkForm').submit( function(e){
    checkDomain();
    e.preventDefault();
});

function checkDomain(){

    apiRequest('GET', '/resources/domains/check?domain='+$('#domain').val(), null, function(){
        ons.notification.alert('Domain name is available for purchase.').then( function(){ });

    }, function(){
        ons.notification.alert('Domain name is not available for purchase.').then( function(){ });

    }, function(){
        ons.notification.alert('The request timed out').then( function(){ });

    });
    
}