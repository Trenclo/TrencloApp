var pullHook = document.getElementById('pull-hook');
pullHook.onAction = function(done){
    getDomains();
    done();
};

function getDomains(){
    $('#data-progress-bar').show();

    apiRequest('GET', '/resources/domains/', [], function(data){
        $('#data-progress-bar').hide();
        $('#dataList').text('');

        if(Object.keys(data.data).length > 0){
            $.each(data.data, function(i, item){
                $('#dataList').append('<ons-list-item onclick="load(\'domains/domain\', \'Domain Details\', \''+item.domainid+'\');" tappable><div class="center">'+item.domainname+'</div></ons-list-item>');
            });

        }else{
            $('#dataList').append('<ons-list-item><div class="center">No items available.</div></ons-list-item>');
        }

    }, function(error){
        ons.notification.alert('An error occured while fetching your domain names.').then(function(){
            $('#data-progress-bar').hide();
        });

    }, function(){
        ons.notification.alert('The request timed out. Please try again or check your internet connection.').then(function(){
            $('#data-progress-bar').hide();
        });
    });
}

getDomains();