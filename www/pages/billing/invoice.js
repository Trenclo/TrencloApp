$('#data-progress-bar').show();

apiRequest('GET', '/invoices/'+localStorage.getItem('SelectedObjectID'), [], function(data){
    $('#data-progress-bar').hide();
    $('#dataList').text('');

    if(Object.keys(data.data).length > 0){

        var details = JSON.parse(data.data.details);

        console.log(details);

        $("#invoiceid").text(details.number);
        $("#total").text('').append('&pound;'+details.total/100);
        $("#status").text(details.status).css('text-transform', 'capitalize');;

        $("#payurl").on("click", function(){ ons.notification.alert('Coming soon.'); });
        $("#pdfurl").on("click", function(){ window.open(details.invoice_pdf, "_blank"); });

    }else{
        ons.notification.alert('An error occured while fetching your invoice.').then(function(){
            $('#data-progress-bar').hide();
        });
    }

}, function(error){
    ons.notification.alert('An error occured while fetching your invoice.').then(function(){
        $('#data-progress-bar').hide();
    });

}, function(){
    ons.notification.alert('The request timed out. Please try again or check your internet connection.').then(function(){
        $('#data-progress-bar').hide();
    });
});