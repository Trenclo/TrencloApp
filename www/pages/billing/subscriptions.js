var pullHook = document.getElementById('pull-hook');
pullHook.onAction = function(done){
    getSubscriptions();
    done();
};

function getSubscriptions(){
    $('#data-progress-bar').show();

    apiRequest('GET', '/subscriptions/', [], function(data){
        $('#data-progress-bar').hide();
        $('#dataList').text('');

        if(Object.keys(data.data).length > 0){
            $.each(data.data, function(i, item){
                var details = JSON.parse(item.details);
                
                $('#dataList').append('<ons-list-item onclick="load(\'billing/subscription\', \'Subscription Details\', \''+item.subscriptionid+'\');" tappable><div class="center">'+details.items.data[0].quantity+' &times; '+details.items.data[0].plan.nickname+'</div></ons-list-item>');
            });

        }else{
            $('#dataList').append('<ons-list-item><div class="center">No items available.</div></ons-list-item>');
        }

    }, function(error){
        ons.notification.alert('An error occured while fetching your subscriptions.').then(function(){
            $('#data-progress-bar').hide();
        });

    }, function(){
        ons.notification.alert('The request timed out. Please try again or check your internet connection.').then(function(){
            $('#data-progress-bar').hide();
        });
    });
}

getSubscriptions();