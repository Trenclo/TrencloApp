var pullHook = document.getElementById('pull-hook');
pullHook.onAction = function(done){
    getCards();
    done();
};

function getCards(){
    $('#data-progress-bar').show();

    apiRequest('GET', '/paymentmethods/', [], function(data){
        $('#data-progress-bar').hide();
        $('#dataList').text('');

        if(Object.keys(data.data).length > 0){
            $.each(data.data, function(i, item){
                var details = JSON.parse(item.details);
                
                $('#dataList').append('<ons-list-item onclick="load(\'billing/card\', \'Card Details\', \''+item.paymentmethodid+'\');" tappable><div class="center">'+details.billing_details.name+' (**** **** **** '+details.card.last4+' expires '+details.card.exp_month+'/'+details.card.exp_year+')</div></ons-list-item>');
            });

        }else{
            $('#dataList').append('<ons-list-item><div class="center">No items available.</div></ons-list-item>');
        }

    }, function(error){
        ons.notification.alert('An error occured while fetching your payment methods.').then(function(){
            $('#data-progress-bar').hide();
        });

    }, function(){
        ons.notification.alert('The request timed out. Please try again or check your internet connection.').then(function(){
            $('#data-progress-bar').hide();
        });
    });
}

getCards();