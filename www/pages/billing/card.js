$('#data-progress-bar').show();

apiRequest('GET', '/paymentmethods/'+localStorage.getItem('SelectedObjectID'), [], function(data){
    $('#data-progress-bar').hide();
    $('#dataList').text('');

    if(Object.keys(data.data).length > 0){

        var details = JSON.parse(data.data.details);

        console.log(details);

        $("#name").text(details.billing_details.name);
        $("#card").text('**** **** **** '+details.card.last4);
        $("#expiry").text(details.card.exp_month+'/'+details.card.exp_year);
        $("#postcode").text(details.card.country+' '+details.billing_details.address.postal_code);


    }else{
        ons.notification.alert('An error occured while fetching your payment method.').then(function(){
            $('#data-progress-bar').hide();
        });
    }

}, function(error){
    ons.notification.alert('An error occured while fetching your payment method.').then(function(){
        $('#data-progress-bar').hide();
    });

}, function(){
    ons.notification.alert('The request timed out. Please try again or check your internet connection.').then(function(){
        $('#data-progress-bar').hide();
    });
});