var stripe = Stripe('pk_test_Q2jAq0zzP3zc4Asa9lwS1pfJ');

var elements = stripe.elements();

var style = {
    base: {
        color: '#32325d',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
            color: '#aab7c4'
        }
    },
    invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
    }
};

var card = elements.create('card', {style: style});

card.mount('#card-element');

card.on('change', function(event) {
    if (event.error) {
        ons.notification.toast(event.error.message, {timeout: 2000});
    }
});

$('#paymentForm').submit( function(e){
    e.preventDefault();
    
    stripe.createToken(card).then(function(result) {
        if (!result.error) {
            saveData(result.token);
        } else {
            ons.notification.toast(result.error.message, {timeout: 2000});
        }
    });
});

// Submit the form with the token ID.
function saveData(token) {
    ons.notification.toast('Got payment method with ID: '+token.id);
}

$(document).ready(function(){
    if(localStorage.getItem('Region') == "Test"){
        $('#testDetails').show();
    }
});