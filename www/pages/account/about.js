var clientData = JSON.parse(localStorage.getItem('Client'));

$('#regionname').text(clientData.regioninfo.regionname);
$('#companyname').text(clientData.regioninfo.companyname);
$('#vatnumber').text(clientData.regioninfo.vatnumber);

$('#supportemail').text('').append('<ons-button modifier="quiet" onclick="window.open(\'mailto:'+clientData.regioninfo.supportemail+'\', \'_self\')">'+clientData.regioninfo.supportemail+'</ons-button>');
$('#supportphone').text('').append('<ons-button modifier="quiet" onclick="window.open(\'tel:'+clientData.regioninfo.supportphone+'\', \'_self\')">'+clientData.regioninfo.supportphone+'</ons-button>');

$('#companyaddress').text('').append('<ons-button modifier="quiet">View on Google Maps</ons-button>')

$('#companyaddress ons-button').on("click", function(e){
    if(typeof nw === 'object'){
        nw.Shell.openExternal('https://www.google.com/maps?q='+clientData.regioninfo.companyname+', '+clientData.regioninfo.companyaddress);
    }else{
        window.open('https://www.google.com/maps?q='+clientData.regioninfo.companyname+', '+clientData.regioninfo.companyaddress, '_system');
    }

    e.preventDefault();
});
