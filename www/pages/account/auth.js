//localStorage.removeItem('AccessToken');
localStorage.removeItem('Client');
localStorage.removeItem('Tenant');
localStorage.removeItem('User');

$('#menu').removeAttr('swipeable').hide();
$('#notifications').removeAttr('swipeable').hide();
$('#menuButton, #notificationsButton, #accountButton').hide();

$(document).ready(function(){

    if(localStorage.hasOwnProperty('Region') && localStorage.hasOwnProperty('API_URL')){

        apiRequest('GET', '/users/me', [], function(data){
            window.open('./index.html', '_self');
        }, function(error){  });

    }else{
        load('account/region', 'Select Region');
    }

});

var retries = 0;

function showAuthBox(){

    // app installed on device, inappbrowser used
    if(typeof cordova === 'object'){

        var authWin = cordova.InAppBrowser.open(localStorage.getItem('AUTHORIZATION_URL')+'?response_type=code&client_id='+localStorage.getItem('APP_CLIENT_ID')+'&redirect_uri='+localStorage.getItem('APP_REDIRECT_URI')+'&state='+localStorage.getItem('OAUTH2_STATE')+'&scope='+localStorage.getItem('API_SCOPES')+'&code_challenge='+localStorage.getItem('BASE64_CODE')+'&code_challenge_method=S256', '_blank', 'location=yes,fullscreen=no,toolbar=yes');


    // running on nw.js, popup used
    }else if(typeof nw === 'object'){

        // show loading modal

        var authWin = window.open(localStorage.getItem('AUTHORIZATION_URL')+'?response_type=code&client_id='+localStorage.getItem('APP_CLIENT_ID')+'&redirect_uri='+localStorage.getItem('APP_REDIRECT_URI')+'&state='+localStorage.getItem('OAUTH2_STATE')+'&scope='+localStorage.getItem('API_SCOPES')+'&code_challenge='+localStorage.getItem('BASE64_CODE')+'&code_challenge_method=S256', '_blank', 'height=700,width=450,menubar=no,resizable=no,status=no,titlebar=no,toolbar=no,location=no,scrollbars=no');

        window.addEventListener('message', message => {
            var data = parseParams(message.data);

            if(message.origin == localStorage.getItem('AUTH_ORIGIN')){
                console.log(data);
        
                if(localStorage.getItem("OAUTH2_STATE") == data.state){
                    
                    var jqxhr = $.post(
                        localStorage.getItem('TOKEN_URL'),
                        {
                            'grant_type': 'authorization_code',
                            'code': data.code,
                            'client_id': localStorage.getItem('APP_CLIENT_ID'),
                            'code_verifier': localStorage.getItem('BASE64_CODE')
                        },
                        function(data){
        
                            localStorage.setItem('AccessToken', JSON.stringify(data));

                            //ons.notification.alert('Sign in success!').then(function(){
                                //ons.notification.alert(localStorage.getItem('AccessToken')).then(function(){
                                    window.open('./index.html', '_self');
                                //});
                            //});
        
                        }
                    ).fail(function(jqxhr, status) {
                            if(retries != 1){
                                showAuthBox();

                                retries = 1;
                            }else{
                                console.warn('Auth Error: Failed to retrieve access token.');
                                ons.notification.alert('Sign in failed, please try again!');

                                retries = 0;
                            }
                        }
                    );
        
                }else{
                    if(retries != 1){
                        showAuthBox();

                        retries = 1;
                    }else{
                        console.warn('Auth Error: Failed to sign in due to invalid state.');
                        ons.notification.alert('Sign in failed, please try again!');
                        
                        retries = 0;
                    }
                }

                authWin.close();

            }else{
                console.warn('Auth Error: Message posted from invalid origin.');
            }
        });
        

    // web browser, redirect used
    }else{
        window.open(localStorage.getItem('AUTHORIZATION_URL')+'?response_type=code&client_id='+localStorage.getItem('WEB_CLIENT_ID')+'&redirect_uri='+localStorage.getItem('WEB_REDIRECT_URI')+'&state='+localStorage.getItem('OAUTH2_STATE')+'&scope='+localStorage.getItem('API_SCOPES')+'&code_challenge='+localStorage.getItem('BASE64_CODE')+'&code_challenge_method=S256', '_self');
    }

}