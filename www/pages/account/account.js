var userData = JSON.parse(localStorage.getItem('User'));
var tenantData = JSON.parse(localStorage.getItem('Tenant'));
var clientData = JSON.parse(localStorage.getItem('Client'));

$('#name').text(userData.fullname);
$('#email').text(userData.emailaddress);
$('#usertype').text(userData.usertype);

if(localStorage.getItem('Theme') == 'dark'){
    $('#darkmode').attr('checked', '');
}

$('#darkmode').on("change", function(event){
    if(document.getElementById('darkmode').checked){
        setTheme('dark');
    }else{
        setTheme('light');
    }
});

$('#title').text(tenantData.title);
$('#tenanttype').text(tenantData.tenanttype);
$('#billingemail').text(tenantData.billingemail);
$('#region').text(clientData.regioninfo.regionname);
$('#vatnumber').text(tenantData.vatnumber);

if((tenantData.tenanttype == "System" || tenantData.tenanttype == "Partner") && (userData.usertype == "Admin" || userData.usertype == "Partner")){
    $('#partnerListItem').show();
}