localStorage.setItem('LSTEST', 1);

localStorage.setItem('BASE64_CODE', Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 20));
localStorage.setItem('OAUTH2_STATE', Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 20));

function load(page, pageTitle, id = 0){

    localStorage.setItem('SelectedObjectID', id);

    req = new XMLHttpRequest();
    req.open('GET', 'pages/'+page+'.html');
    req.send();
    req.onload = () => {
        if(pageTitle){
            $('#pageTitle').text(pageTitle);
        }else{
            $('#pageTitle').text('Trenclo Admin');
        }

        $('#view').html(req.responseText);

        $('#pageScript').remove();
        $('head').append('<script src="pages/'+page+'.js" id="pageScript"></script>');
        
        window.location.hash = '/'+page;

    };
    req.onerror = () => {
        load('404', 'Page Not Found');
    };

}

$(document).ready( function(){

    if(localStorage.hasOwnProperty('LSTEST')){

        if(localStorage.hasOwnProperty('Region')){

            $('#regionScript').remove();
            $('head').append('<script src="assets/js/config-'+localStorage.getItem('Region')+'.js" id="regionScript"></script>');

            const stripe = Stripe(localStorage.getItem('STRIPE_PK'));
            
            if(localStorage.hasOwnProperty('AccessToken')){

                apiRequest('GET', '/clients/me', [], function(data){

                    localStorage.setItem('Client', JSON.stringify(data.data));

                    apiRequest('GET', '/users/me', [], function(data){

                        localStorage.setItem('User', JSON.stringify(data.data));

                        apiRequest('GET', '/tenants/me', [], function(data){

                            localStorage.setItem('Tenant', JSON.stringify(data.data));

                            if(localStorage.hasOwnProperty('TutorialSkipped')){
                                load('home', 'Dashboard');
                            }else{
                                load('account/welcome', 'Getting Started');
                            }

                        }, function(error){
                            load('account/auth');
                            
                        }, function(){
                            ons.notification.alert('The request timed out. Please try again or check your internet connection.').then(function(){
                                window.open('./offline.html', '_self');
                            });
                        });

                    }, function(error){
                        load('account/auth');

                    }, function(){
                        ons.notification.alert('The request timed out. Please try again or check your internet connection.').then(function(){
                            window.open('./offline.html', '_self');
                        });
                    });

                }, function(error){
                    load('account/auth');

                }, function(){
                    ons.notification.alert('The request timed out. Please try again or check your internet connection.').then(function(){
                        window.open('./offline.html', '_self');
                    });
                });

            }else{
                load('account/auth');
            }

        }else{
            load('account/auth');
        }

    }else{
        load('badclient', 'Client Not Supported');
    }

    if(typeof phonegap == 'object'){
        if (ons.platform.isIPhoneX()) {
            $('html').attr('onsflag-iphonex-landscape', '');
            $('html').attr('onsflag-iphonex-portrait', '');
        }
    }

});

window.setInterval( function(){

    if(localStorage.hasOwnProperty('AccessToken')){

        if($(window).width() > 1200){

            $('#menu').removeAttr('collapse').removeAttr('swipeable').show();;
            $('#menuButton').show();

            $('#notifications').removeAttr('collapse').removeAttr('swipeable').show();;
            $('#notificationsButton').show();

            $('.fab').css('right', 240);

        }else{
            $('#menu').attr('collapse', '').attr('swipeable', '').show();;
            $('#menuButton').show();
            
            $('#notifications').attr('collapse', '').attr('swipeable', '').show();;
            $('#notificationsButton').show();

            $('.fab').css('right', 20);
        }

    }

    // if window width and height above a certain level
    // remove "collapse" option from menu
    // hide menu button
    // reverse if below certain height

}, 100);

function setTheme(theme){

    if(theme){
        if(theme == 'dark'){
            localStorage.setItem('Theme', 'dark');
        }else{
            localStorage.setItem('Theme', 'light');
        }

        setTheme();

    }else{
        if(localStorage.getItem('Theme') == 'dark'){
            $('#theme').attr('href', './assets/OnsenUI-dist-2.11.1/css/dark-onsen-css-components.min.css');
        }else{
            $('#theme').attr('href', './assets/OnsenUI-dist-2.11.1/css/onsen-css-components.min.css');
        }
    }

}

setTheme();
