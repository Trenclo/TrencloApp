function parseParams(querystring){
    const params = new URLSearchParams(querystring);

    const obj = {};

    for (const key of params.keys()) {
        if (params.getAll(key).length > 1) {
            obj[key] = params.getAll(key);
        } else {
            obj[key] = params.get(key);
        }
    }

    return obj;
};

function apiRequest(method, path, data, successCallback, errorCallback, timeoutCallback){

    if(localStorage.hasOwnProperty('AccessToken')){

        var access_token = (JSON.parse(localStorage.getItem('AccessToken'))).access_token;

        $.ajax({
            method: method,
            cache: false,
            dataType: 'json',
            data: data,
            timeout: 10000,
            crossDomain: true,
            url: localStorage.getItem('API_URL')+path,
            beforeSend: function (xhr){
                xhr.setRequestHeader("Authorization", "Bearer "+access_token);
            },
            success: function(data){
                successCallback(data);
            },
            error: function(error, textStatus){
                if(textStatus === 'timeout'){
                    timeoutCallback();
                }else{
                    errorCallback(error);
                }
            }
        });

    }else{
        errorCallback('Not signed in.');
    }

}

function getFeedback(){
    ons.notification.confirm('Do you want to leave feedback?').then(function(confirmSend){

        if(confirmSend){
            
            ons.notification.prompt('Please enter your feedback.').then(function(data){

                if(data){

                    // send feedback

                }
    
            });
    
        }

    });
}

function openInSystemBrowser(url){
    if(typeof cordova === 'object'){
        window.open(url, '_system');

    }else if(typeof nw === 'object'){
        alert(url);

    }else{
        window.open(url, '_blank');
    }
}